# Decentralized EXchange Proof of Concept

[![pipeline status](https://gitlab.com/oax/dex-poc/badges/master/pipeline.svg)](https://gitlab.com/oax/dex-poc/commits/master)
[![codecov](https://codecov.io/gl/oax/dex-poc/branch/master/graph/badge.svg?token=EI9o4ZWtb6)](https://codecov.io/gl/oax/dex-poc)

## Documentation

For more details, you can check:

- [Dex documentation](./dex/README.md)
- [chain-dsl documentation](./chain-dsl/README.md)

## Install

Prerequisite on both macOS and Linux:
- [Nix 2.0](https://nixos.org/nix/) package manager

All commands should be run within a `nix-shell --pure` to ensure no
interference with the globally available software packages.

By default Nix is hooking itself into `bash`, so you if you have a different
shell make sure you run `bash -l` to get a bash login shell.

```
cd ~/
git clone https://gitlab.com/oax/dex-poc

cd ~/dex-poc
bash -l
nix-shell --pure

cd ~/dex-poc/chain-dsl
pnpm install

cd ~/dex-poc/dex
pnpm install
direnv allow
```

## Run

```
cd ~/dex-poc/dex
overmind start
```

## Contribute

We are following the [Trunk Based Development](http://trunkbaseddevelopment.com)
model.

Our trunk is the `master` branch and we have one generic `release` branch.

Day-to-day development happens directly on `master`, optionally using
*short-lived* feature branches or
[branch by abstraction](https://www.branchbyabstraction.com)

To cut a release, `master` should be merged into the `release` branch
if the core team generally agrees.

To synchronize with marketing schedules, we should use
[feature toggles](https://trunkbaseddevelopment.com/feature-flags/)
to reveal only the desired amount of functionality.

## CI

Our CI setup uses GitLab to orchestrate builds.
It tests and deploys every commit made in the 2 main branches:

- `master` is deployed under https://oax.gitlab.io/dex-poc/master
- `release` is deployed under https://oax.gitlab.io/dex-poc/release

For convenience and flexibility there is a world-wide CDN distribution
in place which serves the latest [DEX demo release](https://oax.gitlab.io/dex-poc/release) under a shorter, more memorable URL:

* https://dex.demo.oax.org

_Note: The CDN servers cache the application, hence there might be a 15 minutes
delay before changes on https://oax.gitlab.io/dex-poc/release appear
under https://dex.demo.oax.org_

### CI runner

GitLab is capable of farming out the actual integration process to
so called [GitLab runners](https://docs.gitlab.com/runner/).

While gitlab.com offers GitLab runners for free, their performance
and availability is not very high, so we are hosting our own.

To setup a runner using NixOS, it has to be registered by providing
the registration token (shown as `XX-XXXXXXXXXX` in the example below)
from the https://gitlab.com/groups/oax/-/settings/ci_cd page.

Example output
```
# nix-shell -p gitlab-runner
these paths will be fetched (95.23 MiB download, 442.87 MiB unpacked):
...

[nix-shell:~]# gitlab-runner register
Running in system-mode.

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com/
Please enter the gitlab-ci token for this runner:
XX-XXXXXXXXXX
Please enter the gitlab-ci description for this runner:
[ip-123-123-123-123.ap-southeast-1.compute.internal]: oax-aws
Please enter the gitlab-ci tags for this runner (comma separated):

Whether to lock the Runner to current project [true/false]:
[true]: false
Registering runner... succeeded                     runner=xx-XXXXX
Please enter the executor: shell, ssh, virtualbox, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, docker-ssh:
docker
Please enter the default Docker image (e.g. ruby:2.1):
lnl7/nix
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

This creates a `/etc/gitlab-runner/config.toml` file. To share the nix store
with the host we need to add/edit the lines for `environment`, `pre_build_script`
and `volumes`.

```
concurrent = 1
check_interval = 0

[[runners]]
  name = "oax-aws"
  url = "https://gitlab.com/"
  token = "*************"
  executor = "docker"
  environment = ["NIX_REMOTE=daemon", "NIX_PATH=nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos"]
  pre_build_script = "export PATH=$PATH:/run/current-system/sw/bin unset SSL_CERT_FILE; unset GIT_SSL_CAINFO; unset NIX_SSL_CERT_FILE"
  [runners.docker]
    tls_verify = false
    image = "nixos/nix"
    privileged = false
    disable_cache = false
    volumes = ["/nix:/nix:ro", "/run/current-system/sw:/run/current-system/sw:ro", "/nix/var/nix/daemon-socket/socket:/nix/var/nix/daemon-socket/socket", "/etc/ssl/certs:/etc/ssl/certs:ro", "/etc/static/ssl/certs:/etc/static/ssl/certs:ro", "/var/db/gitlab-runner-cache:/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
  [runners.cache]
```

We recommend to increase the `concurrent` option to utilize all CPU cores.

To activate the runner, should only add the following to
the `/etc/nixos/configuration.nix`:

```
  services.gitlab-runner.enable = true;
  services.gitlab-runner.configFile = /etc/gitlab-runner/config.toml;
  virtualisation.docker.enable = true;
```

_Note: A complete example file can be fund in [ci.nix](./ci.nix)_

The logs of the `gitlab-runner` service can be inspected with:

```
journalctl -u gitlab-runner -f
```

If the `config.toml` is changed, the `gitlab-runner` service can be
restarted with:

```
systemctl restart gitlab-runner
```

# Demo Rinkeby node

Our https://dex.demo.oax.org is talking to the smart-contracts on the Rinkeby
test network using the web3 client injected by MetaMask.
To distribute the order book in a decentralized manner, the Dex web app
requires an Ethereum node which has enabled Whisper support.
Infura.io doesn't support Whisper yet as of 2018-06-24, so for convenience
we setup a Whisper-enabled node under https://rinkeby.demo.oax.org, using
NixOS and our Dex points to that node.

You don't have to rely on this machine however, if you run the Dex for yourself.
Just point the `chainUrl` option in the [./dex/src/config.js]() file to your
own Whisper-enabled, Ethereum node.

You can setup such node easily for yourself if you are using NixOS.
On AWS you can follow the official documentation:
https://nixos.wiki/wiki/Install_NixOS_on_Amazon_EC2

Then just add the content of the [./geth.nix] file into your
`/etc/nixos/configuration.nix` and replace the occurances of
`rinkeby.demo.oax.org` with your own DNS name.

Currently the path to the exact version of the `geth` exacutable is
hardwired into the configuration.

To make sure that version is installed on the system, we just ran:

```
rsync -vax ./shell.nix root@rinkeby.demo.oax.org:
ssh root@rinkeby.demo.oax.org "nix-shell --run 'which geth'"
```

Then copy the printed path into your `configuration.nix` onto the `ExecStart`
line.

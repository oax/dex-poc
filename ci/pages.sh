#!/usr/bin/env bash
set -e

mkdir -p public
cd dex

function get_rev() {
    local branch="$1"
    local count=$(git rev-list --count origin/"$branch")
    local sha=$(git rev-parse --short origin/"$branch")
    if [ "$branch" == release ]; then
        prefix=""
    else
        prefix="$branch-"
    fi
    echo "$prefix$count-$sha"
}

function deploy() {

    local branch="$1"
    local deploy_dir=../public/"$branch"

    git checkout -f origin/"$branch"
    pnpm install --production
    cp env/staging.js src/config.js
    mkdir -p "$deploy_dir"
    cp -Lr src/* "$deploy_dir"

    get_rev $branch > "$deploy_dir"/version
}

deploy master
deploy release

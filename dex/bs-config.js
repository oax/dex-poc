module.exports = {
  logPrefix: require('./package.json').name,
  // Files to monitor for changes
  files: [
    'src/*.html',
    'src/**/*.js',
    'net/**/*.json',
    'src/*.css'
  ],
  // Union of directories to serve under the root path in the browser
  server: {
    baseDir: ['./src']
  },

  ghostMode: false
}

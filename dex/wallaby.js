module.exports = function (wallaby) {
  return {
    files: [
      'contracts/**/*.sol',
      'src/**/*.js',
      'test/mocha.opts',
      'solc-input.json'
    ],

    tests: [
      'test/**/*_test.js'
    ],
    env: {
      type: 'node'
    },
    testFramework: 'mocha',
    setup: function (wallaby) {
      // FIXME This timeout is set in `test/mocha.opts` already,
      // but has no effect in Wallaby
      wallaby.testFramework.timeout(5000)
    },
    compilers: {
      '**/*.js': wallaby.compilers.babel()
    }
  }
}

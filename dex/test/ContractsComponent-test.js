/* eslint-env mocha */

import { expect } from 'chain-dsl/test/helpers'
import { address } from 'chain-dsl/es6'
import { NodejsContractLoader } from '../src/NodejsContractLoader'
import { ContractsComponent } from '../src/ContractsComponent'

describe('ContractsComponent', async function () {
  let web3, newContracts

  before(async () => {
    web3 = new Web3(devChainUrl)
    newContracts = new ContractsComponent({
      Loader: NodejsContractLoader,
      contractNames: ['xchg'],
      web3
    })
  })

  it('#start()', async () => {
    const contracts = await newContracts.start()

    expect(contracts)
      .to.be.an.instanceof(ContractsComponent)
      .that.has.property('xchg')

    expect(address(contracts.xchg)).to.satisfy(Web3.utils.isAddress)
  })

  describe('#stop()', async () => {
    it('removes contracts', async () => {
      const startedContracts = await newContracts.start()
      const stoppedContracts = await startedContracts.stop()

      expect(stoppedContracts)
        .to.be.an.instanceof(ContractsComponent)
        .that.does.not.have.property('xchg')
    })
  })
})

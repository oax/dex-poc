/* eslint-env mocha */

import {expect} from 'chain-dsl/test/helpers'
import {
  clone,
  merge,
  Lifecycle,
  start,
  stop,
  // mixinLifecycle,
  dependencies,
  using,
  assocDependency,
  assocDependencies,
  // updateSystem,
  SystemMap
} from '../src/component'

describe('Component library', function () {
  describe('clone()', function () {
    it('is independent from the original', async () => {
      const orig = {a: 1}

      const aClone = clone(orig)

      expect(aClone).property('a', 1)
      orig.a = 'changed'
      expect(aClone).property('a', 1)
    })

    it('is shallow', async () => {
      const orig = {a: {b: 1}}

      const aClone = clone(orig)

      expect(aClone).not.equal(orig)
      expect(aClone.a).equal(orig.a)
      orig.a.b = 'changed'
      expect(aClone.a).property('b', 'changed')
    })

    it('copies getters', () => {
      const orig = {
        get a () { return 123 },
        b: 'asd'
      }
      const aClone = clone(orig)

      expect(aClone).eql({a: 123, b: 'asd'})
    })

    it('preserves prototype', async () => {
      class Something {
        constructor (opts) { Object.assign(this, opts) }
      }

      const orig = new Something({
        get a () { return 123 },
        b: 'asd'
      })
      const aClone = clone(orig)

      expect(aClone).eql({a: 123, b: 'asd'})
      expect(aClone).instanceOf(Something)
    })
  })

  describe('dependencies()', function () {
    it('exist', async () => {
      const DEPS = ['a', 'b']
      const comp = using({data: 123}, DEPS)
      expect(dependencies(comp)).equal(DEPS)
    })

    it('missing', async () => {
      const comp = {data: 123}
      expect(dependencies(comp)).eql({})
    })
  })

  describe('assocDependency()', function () {
    it('works', async () => {
      const sys = new SystemMap({
        comp1: {name: 'comp1'},
        comp2: using(
          {name: 'comp2'},
          ['compX']),
        async start () {
          console.log('starting')
        }
      })
      expect(assocDependency(sys, sys.comp2, 'compX', 'comp1'))
        .property('compX')
        .equals(sys.comp1)
    })
  })

  describe('assocDependencies()', function () {
    it('component', async () => {
      const sys = new SystemMap({
        comp1: {name: 'comp1'},
        comp2: using(
          {name: 'comp2'},
          {comp1: 'comp1'}),
        async start () {
          console.log('starting')
        }
      })

      expect(sys.comp2).to.not.have.property('comp1')
      expect(assocDependencies(sys.comp2, sys))
        .that
        .have
        .property('comp1')
        .eql(sys.comp1)
    })
  })

  describe('SystemMap', function () {
    it('#start() fails', async () => {
      const sys = new SystemMap()
      await expect(sys.start()).eventually.rejectedWith(/not implemented/)
    })

    it('#stop() fails', async () => {
      const sys = new SystemMap()
      await expect(sys.stop()).eventually.rejectedWith(/not implemented/)
    })
  })

  describe('start()', function () {
    it('undefined', async () => {
      await expect(start(undefined)).to.eventually.be.undefined
    })

    it('null', async () => {
      await expect(start(null)).to.eventually.be.null
    })

    it('boolean', async () => {
      await expect(start(true)).to.eventually.be.true
      await expect(start(false)).to.eventually.be.false
    })

    it('object', async () => {
      const obj = {a: 1}
      await expect(start(obj)).to.eventually.equal(obj)
    })

    it('array', async () => {
      const arr = [1, 2, 3]
      await expect(start(arr)).to.eventually.equal(arr)
    })

    it('component', async () => {
      class DummyComponent extends Lifecycle {
        constructor (id) {
          super()
          this.id = id
        }

        async start () {
          return merge(this, {started: this.id})
        }
      }

      const aComponent = new DummyComponent(123)
      await expect(start(aComponent))
        .to.eventually.have.property('started', 123)
    })
  })

  describe('stop()', function () {
    it('undefined', async () => {
      await expect(stop(undefined)).to.eventually.be.undefined
    })

    it('null', async () => {
      await expect(stop(null)).to.eventually.be.null
    })

    it('boolean', async () => {
      await expect(stop(true)).to.eventually.be.true
      await expect(stop(false)).to.eventually.be.false
    })

    it('object', async () => {
      const obj = {a: 1}
      await expect(stop(obj)).to.eventually.equal(obj)
    })

    it('array', async () => {
      const arr = [1, 2, 3]
      await expect(stop(arr)).to.eventually.equal(arr)
    })

    it('component', async () => {
      class DummyComponent extends Lifecycle {
        constructor (id) {
          super()
          this.id = id
        }

        async start () {
          return merge(this, {started: this.id})
        }

        async stop () {
          delete this.started
          return this
        }
      }

      const aComponent = new DummyComponent(123)
      await start(aComponent)

      await expect(stop(aComponent))
        .to.eventually.not.have.property('started')
    })
  })
})

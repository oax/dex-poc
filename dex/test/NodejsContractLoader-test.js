/* eslint-env mocha */

import {expect} from 'chain-dsl/test/helpers'
import {NodejsContractLoader} from '../src/NodejsContractLoader'

describe('NodejsContractLoader', function () {
  it('loads existing contract', async () => {
    await expect(NodejsContractLoader('1337')('weth'))
      .eventually.have.property('name', 'WETH9')
  })

  it('throws when contract is missing', async () => {
    await expect(NodejsContractLoader('1337')('asd'))
      .rejectedWith(/exist.*1337\/asd.json/)
  })
})

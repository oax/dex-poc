/* eslint-env mocha */

import Storage from 'dom-storage'
import sinon from 'sinon'
import { expect } from 'chain-dsl/test/helpers'
import { waitForConnection } from '../../lib/chain-dsl/es6'
import { address } from '../../../chain-dsl/es6'

import { NodejsContractLoader as ContractLoader } from '../../src/NodejsContractLoader'
import * as Contracts from '../../src/dex/Contracts'
import * as Demo from '../../src/dex/Demo'
import * as TradeHistory from '../../src/dex/TradeHistory'

import * as ContractLog from '../../src/dex/ContractLog'
import { WaitForMutation } from '../../src/dex/streams/VuexMutations'

import { config } from '../../src/config.js'

describe('TradeHistory', () => {
  let chainUrl, web3, contracts, DEPLOYER, ALICE, BOB, store, tradeHistory, contractEvents, exchange, mock,
    historySize, netId, storage, contractLog

  function createDummyEvent (overrides = {}) {
    const {base, quote} = store.state.selectedPair
    const tokenPairHash = Web3.utils.soliditySha3(quote.address, base.address)
    const event = {
      'blockNumber': 0,
      'transactionHash': Web3.utils.randomHex(64),
      'blockHash': Web3.utils.randomHex(64),
      'returnValues': {
        'maker': ALICE,
        'taker': BOB,
        'makerToken': '0x1111111111111111111111111111111111111111',
        'takerToken': '0x2222222222222222222222222222222222222222',
        'filledMakerTokenAmount': '1000000000000',
        'filledTakerTokenAmount': '1000000000000',
        'tokens': tokenPairHash,
        'orderHash': Web3.utils.randomHex(64)
      },
      'event': 'LogFill'
    }

    return R.mergeDeepRight(event, overrides)
  }

  before(async () => {
    chainUrl = devChainUrl
    web3 = new Web3(chainUrl)
    await waitForConnection(web3)

    ;[DEPLOYER, ALICE, BOB] = await web3.eth.getAccounts()
    const loader = ContractLoader(await web3.eth.net.getId())
    const deployedContracts = await Contracts.load(loader, web3)
    ;({contracts} = await Demo.ensure(web3, deployedContracts,
      {DEPLOYER, ALICE, BOB}))

    netId = config.networkId
    storage = new Storage(null, {strict: true})
  })

  beforeEach(async () => {
    store = new Vuex.Store({
      state () {
        return {
          selectedPair: {
            base: {address: address(contracts.oax)},
            quote: {address: address(contracts.weth)}
          }
        }
      },
      strict: true
    })

    exchange = contracts.xchg
    historySize = 2

    contractLog = ContractLog.create({
      web3,
      exchange,
      netId,
      storage,
      pollInterval: 20
    })
    await ContractLog.start(contractLog)
    contractEvents = contractLog.events

    tradeHistory = TradeHistory.create({store, contracts, web3, historySize, contractEvents})
    tradeHistory = TradeHistory.start(tradeHistory)
  })

  afterEach(() => {
    TradeHistory.stop(tradeHistory)
    if (mock) { mock.restore() }
    ContractLog.stop(contractLog)
  })

  it('get past transactions from chain', async () => {
    const dummyEvents = [
      createDummyEvent()
    ]

    mock = sinon.mock(exchange)
      .expects('getPastEvents')
      .atLeast(1)
      .resolves(dummyEvents)

    TradeHistory.stop(tradeHistory)

    tradeHistory = TradeHistory.create({store, contracts, web3, historySize, contractEvents})
    tradeHistory = TradeHistory.start(tradeHistory)

    await WaitForMutation(store, ['tradeHistory/addPastTransaction'])

    const pastTransactions = TradeHistory.pastTransactions(tradeHistory)
    expect(pastTransactions).to.containSubset([{
      price: D`1`,
      amount: D`0.000001`,
      event: dummyEvents[0]
    }])

    mock.verify()
  })

  it('monitors LogFill event after start up', async () => {
    const dummyEvents = [
      createDummyEvent({
        returnValues: {
          filledMakerTokenAmount: '2000000000000'
        }
      })
    ]

    mock = sinon.mock(exchange)
      .expects('getPastEvents')
      .atLeast(1)
      .resolves(dummyEvents)

    await WaitForMutation(store, ['tradeHistory/addPastTransaction'])

    const pastTransactions = TradeHistory.pastTransactions(tradeHistory)

    expect(pastTransactions).to.containSubset([{
      price: D`2`,
      amount: D`0.000001`,
      event: dummyEvents[0]
    }])

    mock.verify()
  })

  it('prevents direct mutation of transaction once commited to store', () => {
    const dummyEvent = createDummyEvent()
    const transaction = {
      price: D`1`,
      amount: D`0.000001`,
      event: dummyEvent,
      timestamp: new Date()
    }

    store.commit('tradeHistory/addPastTransaction', transaction)

    expect(_ => { transaction.price = D`2` }).to.throw('Do not mutate')
  })

  it('get one minute trade history', async () => {
    const dummyEvents = [
      createDummyEvent(),
      createDummyEvent({returnValues: {filledMakerTokenAmount: '2000000000000'}}),
      createDummyEvent({returnValues: {filledMakerTokenAmount: '3000000000000'}})
    ]

    mock = sinon.mock(exchange)
      .expects('getPastEvents')
      .atLeast(1)
      .resolves(dummyEvents)

    TradeHistory.stop(tradeHistory)

    tradeHistory = TradeHistory.create({store, contracts, web3, historySize, contractEvents})
    tradeHistory = TradeHistory.start(tradeHistory)

    await WaitForMutation(store, ['tradeHistory/addPastTransaction'])
    await WaitForMutation(store, ['tradeHistory/addPastTransaction'])

    let oneMinHistory = tradeHistory.store.state[tradeHistory.module].oneMinHistory // eslint-disable-line
    mock.verify()
  })

  it('gives history of transactions sorted by timestamp')

  it('ensures that the same event cannot be added twice')
})

/* eslint-env mocha */
/* eslint no-unused-expressions: off */

import { expect } from 'chain-dsl/test/helpers'
import { now } from 'chain-dsl/es6'

import * as Order from '../../../src/dex/Order'
import { OrderExpiry } from '../../../src/dex/streams/OrderExpiry'
import { ZERO_ADDR } from '../../../../chain-dsl/es6'

import { TestScheduler } from '@kwonoj/rxjs-testscheduler-compat'

describe('OrderExpiry', () => {
  let OAX, WETH, scheduler

  before(() => {
    OAX = {
      name: 'Open Asset Exchange',
      symbol: 'OAX',
      address: Web3.utils.randomHex(20)
    }
    WETH = {
      name: 'Wrapped Ether',
      symbol: 'WETH',
      address: Web3.utils.randomHex(20)
    }
  })

  beforeEach(function () {
    scheduler = new TestScheduler()
  })

  it('notifies the subscribers when an order expires', async () => {
    const mockOrder = Order.create({
      address: ZERO_ADDR,
      buy: {amount: D`0.000001`, token: OAX},
      sell: {amount: D`0.000001`, token: WETH},
      exp: now() + 1
    })

    const mockDataSource = Rx.Observable.of(mockOrder)

    let result = null

    OrderExpiry(mockDataSource, scheduler).subscribe(order => {
      result = order
    })

    scheduler.advanceTo(999)
    expect(result).to.be.null

    scheduler.advanceTo(1000)
    expect(scheduler).to.have.property('frame', 1000)
    expect(result).to.eql(mockOrder)
  })

  it('accepts order with expiry longer than 2147483 seconds', async () => {
    const mockOrder = Order.create({
      address: ZERO_ADDR,
      buy: {amount: D`0.000001`, token: OAX},
      sell: {amount: D`0.000001`, token: WETH},
      exp: now() + 2147484
    })

    const mockDataSource = Rx.Observable.of(mockOrder)

    let result = null

    OrderExpiry(mockDataSource, scheduler).subscribe(order => {
      result = order
    })

    scheduler.advanceTo(2147484 * 1000 - 1)
    expect(result).to.be.null

    scheduler.advanceTo(2147484 * 1000)
    expect(scheduler).to.have.property('frame', 2147484 * 1000)
    expect(result).eql(mockOrder)
  })

  it('limits expiry to 90 days', async () => {
    const day = 60 * 60 * 24 // 1 day in secs
    const dayInMs = day * 1000
    const mockOrder = Order.create({
      address: ZERO_ADDR,
      buy: {amount: D`0.000001`, token: OAX},
      sell: {amount: D`0.000001`, token: WETH},
      exp: now() + 120 * day
    })

    const mockDataSource = Rx.Observable.of(mockOrder)

    let result = null

    OrderExpiry(mockDataSource, scheduler).subscribe(order => {
      result = order
    })

    scheduler.advanceTo(90 * dayInMs - 1)
    expect(result).to.be.null

    scheduler.advanceTo(90 * dayInMs)
    expect(scheduler).to.have.property('frame', 90 * dayInMs)

    expect(result).eql(mockOrder)
  })

  it('Order.exp < current system time is expired immediately', async () => {
    const mockOrder = Order.create({
      address: ZERO_ADDR,
      buy: {amount: D`0.000001`, token: OAX},
      sell: {amount: D`0.000001`, token: WETH},
      exp: now() - 2147484 // boundary condition
    })

    const mockDataSource = Rx.Observable.of(mockOrder)

    let result = null

    OrderExpiry(mockDataSource, scheduler).subscribe(order => {
      result = order
    })

    scheduler.advanceTo(1)
    expect(result).to.not.be.null
    expect(scheduler).to.have.property('frame', 1)
    expect(result).eql(mockOrder)
  })
})

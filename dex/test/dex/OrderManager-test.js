/* eslint-env mocha */

import sinon from 'sinon'

import { expect } from 'chain-dsl/test/helpers'
import { address, waitForConnection } from 'chain-dsl'
import Storage from 'dom-storage'
import { NodejsContractLoader as ContractLoader } from '../../src/NodejsContractLoader'
import * as Blotter from '../../src/dex/Blotter'
import * as CancelIntent from '../../src/dex/CancelIntent'
import * as Contracts from '../../src/dex/Contracts'
import * as Order from '../../src/dex/Order'
import * as Orderbook from '../../src/dex/Orderbook'
import * as OrderManager from '../../src/dex/OrderManager'
import * as ContractLog from '../../src/dex/ContractLog'
import { WaitForMutation } from '../../src/dex/streams/VuexMutations'
import { makeChannel } from '../../src/dex/utils'
import * as Fill from '../../src/dex/Fill'
import * as TradeIntent from '../../src/dex/TradeIntent'

function mkIntent (order, intentType) {
  return TradeIntent.create({...order, intentType})
}

describe('OrderManager', function () {
  this.timeout(1000)

  let web3, store, storage, blotter, contracts,
    buyOrder, sellOrderHalf, sellOrderFull,
    ALICE, BOB, OAX, WETH, orderManager, contractLog, netId,
    contractEvents, orderbook, tokenPair

  netId = 1337

  async function createSignedOrder (params) {
    return Order.sign(web3, contracts,
      Order.create({
        address: ALICE,
        buy: {amount: D`0.000001`, token: OAX},
        sell: {amount: D`0.000001`, token: WETH},
        ...params
      })
    )
  }

  // helper functions for tests
  async function settleIntent (tradeIntent, order) {
    const [fillIntent] = Order.matchIntentWithMakerOrders(
      tradeIntent, [{order, fillableSellAmt: order.sell.amount}]
    )
    const transactionHash = await Order.settle(web3, contracts, fillIntent)
    return Fill.create({fillIntent, transactionHash})
  }

  before(async () => {
    web3 = new Web3(devChainUrl)
    await waitForConnection(web3)
    const loader = ContractLoader(netId)
    contracts = await Contracts.load(loader, web3)
    ;[, ALICE, BOB] = await web3.eth.getAccounts()
    const {oax, weth} = contracts
    OAX = {
      name: 'Open Asset Exchange',
      symbol: 'OAX',
      address: address(oax)
    }
    WETH = {
      name: 'Wrapped Ether',
      symbol: 'WETH',
      address: address(weth)
    }
    tokenPair = {
      base: OAX,
      quote: WETH
    }
  })

  beforeEach(async () => {
    store = new Vuex.Store({strict: true})
    storage = new Storage(null, {strict: true})
    blotter = Blotter.create({
      store,
      storage,
      netId,
      sendMessageChannel: makeChannel(),
      receiveMessageChannel: makeChannel()
    })
    blotter = await Blotter.start(blotter)

    orderbook = Orderbook.create({
      store,
      storage,
      netId,
      sendMessageChannel: makeChannel(),
      receiveMessageChannel: makeChannel(),
      tokenPair
    })
    orderbook = await Orderbook.start(orderbook)

    contractLog = ContractLog.create({
      web3,
      exchange: contracts.xchg,
      netId,
      storage,
      pollInterval: 20
    })
    await ContractLog.start(contractLog)
    contractEvents = contractLog.events

    orderManager = OrderManager.create({store, blotter, contracts, web3})
    orderManager = OrderManager.start({...orderManager, orderbook, contractEvents})

    buyOrder = await createSignedOrder()

    sellOrderFull = await createSignedOrder({
      address: BOB,
      buy: buyOrder.sell,
      sell: buyOrder.buy
    })

    sellOrderHalf = await createSignedOrder({
      address: BOB,
      buy: {amount: buyOrder.sell.amount.div(2), token: buyOrder.sell.token},
      sell: {amount: buyOrder.buy.amount.div(2), token: buyOrder.buy.token}
    })
  })

  afterEach(async () => {
    Orderbook.stop(orderbook)
    Blotter.stop(blotter)
    OrderManager.stop(orderManager)
    ContractLog.stop(contractLog)
  })

  it('subscribes', async () => {
    expect(orderManager.subscriptions.length).equal(3)
  })

  describe('in Orderbook', () => {
    beforeEach(async () => {
      Orderbook.insert(orderbook, buyOrder)
    })

    it('reacts to partial fill contract event', async () => {
      const tradeIntent = mkIntent(sellOrderHalf, 'buy')
      await settleIntent(tradeIntent, buyOrder)

      await WaitForMutation(store, ['updateInOrderbook'])
      const updatedOrder = Orderbook.get(orderbook, buyOrder.orderHash)

      expect(updatedOrder.status).equal('partiallyFilled')
    })

    it('reacts to complete fill contract event', async () => {
      const tradeIntent = mkIntent(sellOrderFull, 'buy')
      await settleIntent(tradeIntent, buyOrder)

      await WaitForMutation(store, ['removeFromOrderbook'])
      await expect(Orderbook.contains(orderbook, buyOrder)).to.be.false
    })

    it('removes expired order from Orderbook', async () => {
      const dummyEvents = [
        {
          event: 'LogError',
          returnValues: {
            errorId: '0',
            orderHash: buyOrder.orderHash
          }
        }
      ]

      const mock = sinon.mock(contracts.xchg)
        .expects('getPastEvents')
        .atLeast(1)
        .resolves(dummyEvents)

      try {
        await WaitForMutation(store, ['removeFromOrderbook'])
        expect(Orderbook.contains(orderbook, buyOrder)).equal(false)
      } finally {
        mock.restore()
      }
    })

    it('removes cancelled order', async () => {
      const cancelIntent = CancelIntent.create({
        order: buyOrder,
        cancelAmt: buyOrder.buy.amount
      })
      await Order.cancel(web3, contracts, cancelIntent)

      await WaitForMutation(store, ['removeFromOrderbook'])
      await expect(Orderbook.contains(orderbook, buyOrder)).to.be.false
    })
  })

  describe('in Blotter', () => {
    describe('if settling against smaller order in Orderbook', async () => {
      beforeEach(async () => {
        Orderbook.insert(orderbook, sellOrderHalf)
        Blotter.insert(blotter, buyOrder)
      })

      it('reacts to partial fill event', async () => {
        const tradeIntent = mkIntent(sellOrderHalf, 'buy')
        settleIntent(tradeIntent, buyOrder)

        await WaitForMutation(store, ['blotter/updateRecord'])

        const record = Blotter.get(blotter, buyOrder.orderHash)

        await expect(record.data.filled.buy).eqD('0.0000005')
        await expect(record.data.filled.sell).eqD('0.0000005')
        await expect(record.data.status).equal('partiallyFilled')
      })
    })

    describe('if settling against same amount order in Orderbook', async () => {
      beforeEach(async () => {
        Orderbook.insert(orderbook, sellOrderFull)
        Blotter.insert(blotter, buyOrder)
      })

      it('reacts to full fill event', async () => {
        const tradeIntent = mkIntent(sellOrderFull, 'buy')
        settleIntent(tradeIntent, buyOrder)

        await WaitForMutation(store, ['blotter/updateRecord'])

        const record = Blotter.get(blotter, buyOrder.orderHash)

        expect(record.data.status).equal('fulfilled')
      })
    })

    describe('if settling against fully filled order', async function () {
      this.timeout(10000)
      let otherSellOrder

      beforeEach(async () => {
        otherSellOrder = await createSignedOrder({
          address: BOB
        })

        // fully settle the buy order
        const tradeIntent = mkIntent(sellOrderFull, 'buy')
        await settleIntent(tradeIntent, buyOrder)
      })

      it('settling again reacts with error', async () => {
        const tradeIntent = mkIntent(otherSellOrder, 'buy')
        const fill = await settleIntent(tradeIntent, buyOrder)

        Blotter.insert(blotter, fill)

        await WaitForMutation(store, ['blotter/updateRecord'])

        const record = Blotter.get(blotter, fill.transactionHash)

        expect(record.data.status).equal('error')
      })
    })
  })
})

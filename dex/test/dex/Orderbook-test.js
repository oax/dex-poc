/* eslint-env mocha */

import { TestScheduler } from '@kwonoj/rxjs-testscheduler-compat'

import { expect } from 'chain-dsl/test/helpers'
import { address, waitForConnection } from 'chain-dsl'
import { now, ZERO_ADDR } from 'chain-dsl/es6'
import Storage from 'dom-storage'
import { NodejsContractLoader as ContractLoader } from '../../src/NodejsContractLoader'
import * as Order from '../../src/dex/Order'
import * as Contracts from '../../src/dex/Contracts'
import * as Demo from '../../src/dex/Demo'
import * as Orderbook from '../../src/dex/Orderbook'
import { makeChannel } from '../../src/dex/utils'
import { ceil } from '../../../chain-dsl/es6'
import { WaitForMutation } from '../../src/dex/streams/VuexMutations'
import * as Message from '../../src/dex/messaging/Message'
import { OrderExpiry } from '../../src/dex/streams/OrderExpiry'

describe('Orderbook', async () => {
  let store,
    orderbook,
    storage,
    clock,
    web3,
    contracts,
    order,
    OAX,
    WETH,
    orderParams,
    tokenPair,
    signedOrder,
    DEPLOYER,
    ALICE,
    BOB,
    sendMessageChannel,
    receiveMessageChannel,
    scheduler
  const netId = 9

  before(async () => {
    web3 = new Web3(devChainUrl)
    await waitForConnection(web3)
    ;[DEPLOYER, ALICE, BOB] = await web3.eth.getAccounts()
    const loader = ContractLoader(await web3.eth.net.getId())
    const deployedContracts = await Contracts.load(loader, web3)
    ;({ contracts } = await Demo.ensure(web3, deployedContracts, {
      DEPLOYER,
      ALICE,
      BOB
    }))
    const { oax, weth } = contracts
    OAX = {
      name: 'Open Asset Exchange',
      symbol: 'OAX',
      address: address(oax)
    }
    WETH = {
      name: 'Wrapped Ether',
      symbol: 'WETH',
      address: address(weth)
    }

    tokenPair = {
      base: OAX,
      quote: WETH
    }

    orderParams = {
      address: ALICE,
      buy: { amount: D`0.000001`, token: OAX },
      sell: { amount: D`0.000001`, token: WETH }
    }

    order = Order.withHash(contracts, Order.create(orderParams))
    signedOrder = await Order.sign(web3, contracts, order)

    tokenPair = {
      base: OAX,
      quote: WETH
    }
  })

  beforeEach(async () => {
    store = new Vuex.Store({
      state: () => {
        return {
          selectedPair: tokenPair
        }
      },
      strict: true
    })
    storage = new Storage(null, { strict: true })
    sendMessageChannel = makeChannel()
    receiveMessageChannel = makeChannel()

    scheduler = new TestScheduler()

    orderbook = Orderbook.create({
      store,
      storage,
      netId,
      sendMessageChannel,
      receiveMessageChannel,
      tokenPair,
      web3,
      contracts
    })

    orderbook.orderExpiry = OrderExpiry(orderbook.orderExpirySource, scheduler)
    orderbook = Orderbook.start(orderbook)
  })

  afterEach(() => {
    if (orderbook) orderbook = Orderbook.stop(orderbook)
  })

  afterEach(() => {
    if (clock) {
      clock.restore()
      clock = null
    }
  })

  it('is empty by default', () => {
    expect(store.state.orderbook.orders.length).to.equal(0)
  })

  describe('.insert', () => {
    it('keeps inserted orders immutable', () => {
      const newOrder = clone(order)
      Orderbook.insert(orderbook, newOrder)
      expect(() => {
        newOrder.orderHash = 'invalid-mutation'
      }).to.throw('Do not mutate')
    })

    it('inserts the order into the orderbook', () => {
      Orderbook.insert(orderbook, order)
      expect(Orderbook.get(orderbook, order.orderHash)).to.eql(order)
    })

    it('prevents duplicate orders', () => {
      Orderbook.insert(orderbook, order)
      expect(Orderbook.contains(orderbook, order)).equal(true)
      const newOrder = Object.assign({}, order)
      expect(() => Orderbook.insert(orderbook, newOrder)).throw(
        '[addToOrderbook] Duplicated order'
      )
    })

    it('Throws error on inserting an invalid order', () => {
      const emptyOrder = {}
      expect(() => Orderbook.insert(orderbook, emptyOrder)).throw(
        '[addToOrderbook] Order expired'
      )
    })

    it('Throws error on inserting an order without an order hash', () => {
      expect(() =>
        Orderbook.insert(orderbook, R.omit(['orderHash'], order))
      ).throw('[addToOrderbook] Order hash missing')
    })

    it('can check if an order already exists', () => {
      Orderbook.insert(orderbook, order)
      expect(Orderbook.contains(orderbook, order)).equal(true)
    })
  })

  describe('.update', function () {
    it('works', () => {
      Orderbook.insert(orderbook, order)
      const newOrder = Object.assign({}, order, { propModified: true })
      Orderbook.update(orderbook, newOrder)
      expect(Orderbook.get(orderbook, newOrder.orderHash)).to.eql(newOrder)
    })
  })

  describe('.remove', function () {
    it('works', () => {
      Orderbook.insert(orderbook, order)
      Orderbook.remove(orderbook, order)
      expect(Orderbook.contains(orderbook, order)).equal(false)
    })
  })

  it('removes expired orders automatically', () => {
    const expiringOrder = Object.assign({}, order, { exp: now() + 1 })
    store.commit('addToOrderbook', expiringOrder)
    scheduler.advanceTo(1000)

    expect(Orderbook.contains(orderbook, expiringOrder)).equal(false)
  })

  describe('order aggregation', function () {
    it('aggregates buy orders by price', function () {
      const order1 = Order.withHash(contracts, Order.create(orderParams))
      const order2 = Order.withHash(contracts, Order.create(orderParams))

      Orderbook.insert(orderbook, order1)
      Orderbook.insert(orderbook, order2)

      const price = ceil(Order.withPrice(order1, tokenPair).price.amount, 8)
      expect(store.getters['aggregatedBuyOrders']).to.eql([
        {
          buy: {
            amount: order1.buy.amount.times(D`2`),
            token: order1.buy.token
          },
          sell: {
            amount: order1.sell.amount.times(D`2`),
            token: order1.sell.token
          },
          price: {
            amount: price
          }
        }
      ])
    })

    it('aggregates sell orders by price', function () {
      const order1 = Order.withHash(
        contracts,
        Order.create({
          address: ZERO_ADDR,
          buy: { amount: D`0.000001`, token: WETH },
          sell: { amount: D`0.000001`, token: OAX }
        })
      )
      const order2 = Order.withHash(
        contracts,
        Order.create({
          address: ZERO_ADDR,
          buy: { amount: D`0.000001`, token: WETH },
          sell: { amount: D`0.000001`, token: OAX }
        })
      )

      Orderbook.insert(orderbook, order1)
      Orderbook.insert(orderbook, order2)

      const price = ceil(Order.withPrice(order1, tokenPair).price.amount, 8)

      expect(store.getters['aggregatedSellOrders']).to.eql([
        {
          buy: {
            amount: order1.buy.amount.times(D`2`),
            token: order1.buy.token
          },
          sell: {
            amount: order1.sell.amount.times(D`2`),
            token: order1.sell.token
          },
          price: {
            amount: price
          }
        }
      ])
    })
  })

  context('orderbook requests', function () {
    let getSendMessage, rfobMessage

    before(function () {
      getSendMessage = function (sendMessageChannel) {
        return new Promise(resolve => {
          sendMessageChannel.subscribe(msg => resolve(msg))
        })
      }

      rfobMessage = Message.toRFOBMessage(tokenPair)
    })

    it('can publish orderbook requests', async function () {
      const receivedMessage = getSendMessage(sendMessageChannel)

      Orderbook.requestActiveOrders(orderbook)
      expect(await receivedMessage).to.eql(rfobMessage)
    })

    it('publish orderbook request on start', async function () {
      const receivedMessage = getSendMessage(sendMessageChannel)

      Orderbook.stop(orderbook)
      Orderbook.start(orderbook)

      expect(await receivedMessage).to.eql(rfobMessage)
    })
  })

  it('Incoming orders from MessageBroker enters orderbook', async () => {
    const receivedNewOrder = WaitForMutation(store, ['addToOrderbook'])

    orderbook.receiveMessageChannel.next(Message.toOrderMessage(signedOrder))

    await receivedNewOrder

    const orderFromOrderbook = Orderbook.get(orderbook, signedOrder.orderHash)
    expect(orderFromOrderbook).to.be.an.instanceof(Order.Order)

    expect(orderFromOrderbook).to.eql(signedOrder)
  })

  it('Incoming orders are sanitized before entering orderbook', async () => {
    const receivedNewOrder = WaitForMutation(store, ['addToOrderbook'])

    const orderWithoutBogusEvent = Order.create(signedOrder)

    const orderWithBogusEvent = Order.SignedOrderValidator(
      R.mergeDeepRight(orderWithoutBogusEvent, {
        events: [{ event: 'BogusEvent' }]
      })
    )

    orderbook.receiveMessageChannel.next(
      Message.toOrderMessage(orderWithBogusEvent)
    )

    await receivedNewOrder

    const orderFromOrderbook = Orderbook.get(orderbook, signedOrder.orderHash)
    expect(orderFromOrderbook).to.eql(orderWithoutBogusEvent)
  })

  it('Duplicated incoming orders does not throw', async () => {
    const errorMessage = new Promise(resolve => {
      orderbook.events
        .filter(event => !R.isNil(event.error))
        .subscribe(event => resolve(event))
    })

    orderbook.receiveMessageChannel.next(Message.toOrderMessage(signedOrder))
    orderbook.receiveMessageChannel.next(Message.toOrderMessage(signedOrder))

    expect((await errorMessage).order).to.eql(signedOrder)
    expect((await errorMessage).error).to.match(/Duplicated order/)
  })

  it('rejects incoming order message without signature', async function () {
    const errorMessage = new Promise(resolve => {
      orderbook.events
        .filter(event => !R.isNil(event.error))
        .subscribe(event => resolve(event))
    })

    const orderWithBadError = clone(order)
    delete orderWithBadError.sig

    orderbook.receiveMessageChannel.next(
      Message.toOrderMessage(orderWithBadError)
    )

    expect((await errorMessage).order).to.eql(order)
    expect((await errorMessage).error).to.match(
      /Expected a value of type `signature` for `sig`/
    )
  })

  it('rejects incoming order message with bad signature', async function () {
    const errorMessage = new Promise(resolve => {
      orderbook.events
        .filter(event => !R.isNil(event.error))
        .subscribe(event => resolve(event))
    })

    const orderWithBadError = clone(order)
    orderWithBadError.sig = Web3.utils.randomHex(65)

    orderbook.receiveMessageChannel.next(
      Message.toOrderMessage(orderWithBadError)
    )

    expect((await errorMessage).order).to.eql(orderWithBadError)
    expect((await errorMessage).error).to.match(/invalid Ethereum signature/)
  })

  it('rejects incoming order messages with a wallet address different from the one used to sign', async function () {
    const errorMessage = new Promise(resolve => {
      orderbook.events
        .filter(event => !R.isNil(event.error))
        .subscribe(event => resolve(event))
    })

    const orderWithWrongAddress = clone(signedOrder)
    orderWithWrongAddress.address = BOB

    orderbook.receiveMessageChannel.next(
      Message.toOrderMessage(orderWithWrongAddress)
    )

    expect((await errorMessage).order).to.eql(orderWithWrongAddress)
    expect((await errorMessage).error).to.match(
      /Order message has invalid signature/
    )
  })

  it('rejects incoming order messages with unlisted buy token', async function () {
    const errorMessage = new Promise(resolve => {
      orderbook.events
        .filter(event => !R.isNil(event.error))
        .subscribe(event => resolve(event))
    })

    const orderWithWrongBuyTokenAddress = await Order.sign(
      web3,
      contracts,
      Order.create(
        R.mergeDeepRight(signedOrder, {
          buy: { token: { address: ZERO_ADDR } }
        })
      )
    )

    orderbook.receiveMessageChannel.next(
      Message.toOrderMessage(orderWithWrongBuyTokenAddress)
    )

    expect((await errorMessage).order).to.eql(orderWithWrongBuyTokenAddress)
    expect((await errorMessage).error).to.match(
      /Order contains unlisted token address/
    )
  })

  it('rejects incoming order messages with unlisted sell token', async function () {
    const errorMessage = new Promise(resolve => {
      orderbook.events
        .filter(event => !R.isNil(event.error))
        .subscribe(event => resolve(event))
    })

    const orderWithWrongSellTokenAddress = await Order.sign(
      web3,
      contracts,
      Order.create(
        R.mergeDeepRight(signedOrder, {
          sell: { token: { address: ZERO_ADDR } }
        })
      )
    )

    orderbook.receiveMessageChannel.next(
      Message.toOrderMessage(orderWithWrongSellTokenAddress)
    )

    expect((await errorMessage).order).to.eql(orderWithWrongSellTokenAddress)
    expect((await errorMessage).error).to.match(
      /Order contains unlisted token address/
    )
  })

  it('send active orders in response to RFOB message', async () => {
    Orderbook.insert(orderbook, signedOrder)

    const outgoingOrderMessage = new Promise(resolve => {
      orderbook.sendMessageChannel.subscribe(msg => resolve(msg))
    })

    orderbook.receiveMessageChannel.next(Message.toRFOBMessage(tokenPair))

    const whisperOrder = Order.WhisperOrder(signedOrder)

    expect(await outgoingOrderMessage).to.eql(
      Message.toOrderMessage(whisperOrder)
    )
  })
})

/* eslint-env mocha */

import { expect } from 'chain-dsl/test/helpers'
import 'chain-dsl'
import { waitForConnection, wad2D, balance } from 'chain-dsl/es6'
import {NodejsContractLoader as ContractLoader} from '../../src/NodejsContractLoader'
import * as Demo from '../../src/dex/Demo'
import * as Contracts from '../../src/dex/Contracts'

describe('Demo.ensure', function () {
  let web3, contracts, DEPLOYER, ALICE, BOB

  before('deployment', async () => {
    web3 = new Web3(devChainUrl)
    await waitForConnection(web3)
    ;[DEPLOYER, ALICE, BOB] = await web3.eth.getAccounts()
    const loader = ContractLoader(await web3.eth.net.getId())
    const deployedContracts = await Contracts.load(loader, web3)
    ;({contracts} = await Demo.ensure(web3, deployedContracts, {DEPLOYER, ALICE, BOB}))
  })

  it('deployed', () => {
    expect(contracts).property('xchg')
  })

  it('tops up ALICE ETH balance', async () => {
    const aBN = await balance(web3, ALICE)
    expect(wad2D(aBN)).eqD(D`5`, 7)
  })
})

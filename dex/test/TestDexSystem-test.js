/* eslint-env mocha */

import {expect} from 'chain-dsl/test/helpers'
import * as component from '../src/component'
import {TestDexSystem} from '../src/TestDexSystem'

describe('TestDexSystem', async function () {
  let dex

  before(async () => {
    const newDex = new TestDexSystem()
    dex = await component.start(newDex)
  })

  it('works', async () => {
    expect(dex).instanceof(TestDexSystem)
    expect(dex).property('web3')
  })
})

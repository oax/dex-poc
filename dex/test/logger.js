/* eslint-env mocha */

let loggerState = []

function append (method, ...args) {
  loggerState.push([method, args])
}

let consoleOrig = {}

const hooks = ['info', 'warn', 'debug']

before(function () {
  hooks.forEach(key => { consoleOrig[key] = console[key] })
  hooks.forEach(key => { console[key] = append.bind(console, key) })
})

after(function () {
  hooks.forEach(key => { console[key] = consoleOrig[key] })
})

beforeEach(function () {
  loggerState = []
})

afterEach(function () {
  if (!this.currentTest || this.currentTest.state !== 'passed') {
    loggerState.forEach(([key, args]) => {
      consoleOrig[key].apply(console, args)
    })
  }
})

describe('logger', () => {
  it.skip('shows warning messages on failure', () => {
    console.warn('Warning shown at the end')
    throw new Error('expected failure')
  })

  it("doesn't show error messages on success", () => {
    console.warn('not shown')
  })
})

	OOB: OAX DEX Order Book Protocol
	Version: 0.1
	Status: Work-in-Progress

# Table of Contents
* [Introduction](#introduction)
* [Features](#features)
* [Terminologies](#Terminologies)
* [Transport](#transport)
* [Implementation Overview](#implementation-overview)
* [Message Types](#message-types)
* Status
	* [Release History](#release-history)
	* [Implementation Status](#implementation-status)
* [References](#references)
* [Contributors](#contributors)

# Introduction
OOB (tentative code name) is a decentralized order book protocol for 
decentralized exchange (DEX) in the OAX network to exchange messages. These 
messages allow peers in the OAX network to coordinate their actions such as 
signaling orders and requesting for order books.

The OOB is designed to work with Whisper as a canonical transport layer
technology, but can work with any transport layer protocol that satisfies the
requirements laid out in the [Transport](#transport) section.

Note that the protocol currently exists as a Proof-of-Concept and may be
subjected to substantial changes.

# Features
* Order book aggregation (to come)
* Settlement Path Finding (to come)
* multiple token pair support
* Validation
  * Order signature check
  * Token balance check
  * Token Transfer Approval check

# Terminologies

TODO

# Transport

TODO: outline the requirements for the transport protocol.

[Whisper](https://github.com/ethereum/wiki/wiki/Whisper) is the canonical
transport protocol used by OOB to relay order messages.

### Message Topic

Order messages are advertised and tagged in Whisper with the 32-byte topic 
`0xf63d04b2`. This is derived from taking the first 4 bytes of the Keccak-256
hash of the string `"OAX DEX"`.

```javascript
message_topic := keccak256("OAX DEX")[:4]
```

### Shared Symmetric Key
k
Encryption for OOB is superfluous, and serves only to minimize the impact 
of topic collision with another application.

Messages sent to the message topic `0xf63d04b2` must be encrypted with the 
Keccak-256 hash of the string `"OAX DEX"`.

```javascript
message_topic := keccak256("OAX DEX")
```

### Message Payload Format

Messages carry the same general format, regardless of their type. The payload
is the HEX representation of the UTF-8 stringified JSON object whose 
structure is given by:

```json
{
  "type": <MESSAGE TYPE>,
  ("tokens": <TOKENS HASH>,)
  ("data": <DATA>,)
  "version": <VERSION>
} 
```

* `<MESSAGE TYPE>`: One of `"order"` or `"rfob"` (see
[Message Types](#message-types))
* `<TOKENS HASH>`: Keccak-256 hash of the tokens addresses (see below). 
_Optional_.
* `<DATA>`: The data of the message. _Optional_.
* `<VERSION>`: An integer. The current version is `1`.

The final message payload is the big endian HEX encoding of the 
UTF-8 string representation of JSON payload. For example, given the following 
message:

```json
{
  "type": "rfob",
  "version": 1
} 
```

The string representation of the payload is

```javascript
'{"type":"rfob","version":1}'
```

Note that the single quotes are only there to signify string representation.
Finally, the HEX representation of this string representation is

```
0x7b2274797065223a2272666f62222c2276657273696f6e223a317d
```

### Proof-of-Work Target and Time

TODO

# Message Types

### Order Message

Order messages signal an intention to trade. 

### Request for Order Book (RFOB) Message


# Release History
	
# Implementation Status

## Known Issues

# References

# Contributors

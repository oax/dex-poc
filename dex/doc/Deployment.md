# Deployment

A `dex-poc` Dapp deployment consist of a combination of the following few components:

1. Browser
    1. Metamask browser extension
    1. Dex UI
        1. Web3 client to Ethereum node URL
            1. Account address**es** from node
        1. Web3 client to Metamask
            1. **1** account address from Metamask

1. Dex UI server
    1. Per-user environments
        1. Local @ **file**://... (possible in theory)
        1. Local @ **http**://localhost:3000/ (via `browser-sync` as started by `overmind`)
        1. Keybase KBFS @ **https**://<user>.keybase.io/dex-poc
    1. Per-organization environments
        1. OAX AWS S3 @ **https**://...
           1. CloudFront for HTTPS
              1. Setup Certificate Manager; needs some email confirmations, so requires some MX setup
              1. Might require the limited `oax.org` CloudFlare DNS to delegate a subdomain to AWS
           1. CloudFlare for HTTPS
        1. GitLab pages @ **https**://...
           1. It might be mandatory to create a CI pipeline for publishing changes — David
        1. GitHub pages @ **https**://<org>.github.io/<prj>
           1. the `oax` org is taken, so we can only use
              http://openanx.github.io or http://enumatech.github.io
           1. `https://dex.demo.oax.org` via CNAME + CloudFlare SSL termination can work
              1. Issue: CloudFlare settings access is limited to certain ppl with little time
        1. [Now](https://zeit.co/now) @ **https**://<...>.now.sh
           1. Supports [custom domains](https://zeit.co/docs/features/aliases#custom-domains). [Price]](https://zeit.co/pricing): 15 USD/mo

1. Ethereum node
    1. Locally ran **http**://localhost:8900
        1. **1** specific Ethereum network
        1. Multiple account private keys
            1. Multiple **unlocked** contract addresses
    1. Shared across company, eg.<br>
        **https**://rinkeby.enuma.io,<br>
        **ws**://rinkeby.enuma.io:8546,<br>
        **http**://geth.enuma.io:8545
        1. **1** specific Ethereum network
        1. Multiple account private keys
            1. Multiple **unlocked** contract addresses
    1. Third-party
        1. [Infura](https://infura.io) **https**://
        1. Metamask

1. Ethereum blockchain
    1. Contracts
        1. 0x exchange
        1. 0x exchange proxy
        1. WETH token
        1. OAX token
    1. Demo account
        1. balances
            1. ETH
            1. WETH
            1. OAX
        1. EIP20 allowances for the exchange proxy contract

One challenge is to convince `web3` to use specific private keys for
signing transactions, because the accounts corresponding to those
private keys must have sufficient ether/token balances and allowances.

Another challenge is the _Mixed content_ errors thrown by browsers,
caused by trying to connect from a http**s** web app to a plain http
web3 RPC node.

## Local development

## Rinkeby

Deployment and demo scenario setup requires using specific accounts
which known to have ether balance on the Rinkeby test network.

Currently the code relies on the blockchain node signing transactions,
so we need a local blockchain node connecting to Rinkeby.

To save disk space we should sync a light node which will take up "only"
less than 1GB (as of 2018-04-03).

Running the following script will fire up such a node, which will
1. listen for RPC commands over HTTP port 8420 & WS port 8421 & DEVP2P 30420
2. use the same set of keys from the `keystore` directory as the `geth --dev` chain
3. saves the chain data under `$HOME/rinkeby-light/`

```bash
bin/geth.sh rinkeby light 3
```

0x requires 2 different tokens to be swapped, so for experimental purposes
we can deploy a demo OAX & SWIMUSD token, which is implemented as a DSToken contract.

```bash
bin/deploy-rinkeby-demo-tokens.js
```

The address and JSON interface of the contract can be found under
`net/4/oax.json` & `net/4/swimusd.json`.

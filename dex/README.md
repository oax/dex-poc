# OAX Decentralized eXchange Proof of Concept Application

The project is using `direnv` + `nix` to provide `pnpm`, `node`, `geth`, `solc`, `overmind`.

## Develop

IDEALLY, after pulling new versions, to ensure the latest dependencies are installed,
run the following at the _root of this monorepo_:

```bash
pnpm recursive install
pnpm recursive link
```

There is some bug in the `pnpm` version we are using, so PRACTICALLY,
you have to manually install the dependencies of the `chain-dsl` library
and the `dex` application:

```
cd ../chain-dsl
pnpm install
cd -
pnpm install
```

Run a UI server and some `geth --dev` chains.

```bash
overmind start  # or `overmind s` for short
```

The opening browser page will throw errors at this point because
it's not waiting for development blockchain to completely start,
neither it waits for the contracts being deployed.

Once the deployment is done, the browser will get reloaded automatically.

`overmind` compiles the 0x contracts from the `./contracts/` directory
with `solc` and deploys them into the dev chain.

Deployment addresses and the contract ABIs (aka jsonInterfaces) are saved
under `./net/9`, from where the browser code can pick them up.

There are 2 WETH9 tokens being used as a maker and a taker token.
One is simply called `dex.contracts.weth`, the other is `dex.contracts.oax`

The throw-away, in-memory, `geth --dev --networkid 9`, development blockchain,
is accessible over HTTP RPC on port 8900, so you can point your Metamask
plugin to it via the _Custom RPC_ menu, by entering http://localhost:8900
into the _New RPC URL_ field, _SAVE_ it, then close the dialog with the _X_ icon.

The automated deployment process uses 3 accounts to prepare a test-bed for
manual testing on the development chain:

1. Deployer
1. Alice
1. Bob

These accounts are generated from the HD Wallet mnemonic specified in the
`./bin/import-keys.sh` script, and it's currently:

```
turn nest wrestle cushion wash anchor judge column convince vault cactus decorate
```

In Metamask, you should use the _Create Account_ menu twice to gain access to Alice
and Bob's accounts, so you can put yourself in their shoes and act as them.

Alice has been setup to have some demo OAX tokens and Bob has some WETH.

## Deployment

See the details in [Deployment.md](./doc/Deployment.md).

## Overmind workflow

Mocha seems to monitor `*.js` files 1 depth below the directory it was started in.

Adding `--watch-extensions *.sol` to `test/mocha.opts` would trigger re-running
tests before they could be  compiled and deployed.
Formerly it was a sufficient trigger because the mocha tests were doing both the
compilation and deployment.

Specifying `--watch-extensions *.json` triggers test re-run right after compilation
already (producing `out/contracts.json`), which is also too early.

Emitting a file to `net/9/done.js` for example to serve as a notification signal to
mocha, didn't work either because `mocha --watch` seems to ignore changes in
files excluded by `.gitignore` and are also outside of the `test/` directory.

As a workaround the deployment process (`bin/deploy.js`) emits a `test/deployed-to-9.js`
file to trigger a test re-run, whether they are running from IntelliJ or `pnpm test`.

## Coding conventions

### Logging

Use `console.debug` in frontend code, so when it's ran in
Node.js tests it won't clutter the test reporter's output.
In the browser it won't flood the console either, unless the
logging verbosity is raised in the browser JavaScript console.

### Module structure

We prefer working with plain JavaScript objects and explicitly
pass them as the 1st parameter to functions instead of using classes
and `this.xxx` from methods.

TODO Explain why

For example, something like:

`src/order.js`:

```javascript
export function create(fields) {
    return {fields...}
}

export function hash(order) {
    return doSomethingWith(order.fields...)
}

export default {
    create,
    hash
}
```

vs

```javascript
export class Order {
    constructor(fields) {
        this.fields = fields
    }

    hash() {
        return doSomethingWith(this.fields)
    }
}

export default Order
```

FIXME Double check if this is the case indeed:

If we export individual functions both at the place of their definition and
as an `export default`, then we can import them in two different ways, yet
consume them the same way:

1. `import * as Order from './order.js'`
2. `import Order from './order.js'`

Then we can use `Order.create()` and `Order.hash()`

It seems `nodejs --experimental-modules` only supports importing the
`export default`, hence it might be good to provide the exports both ways.

## Design Notes

### BigNumber.js

Since none of the built-in JavaScript `Number` to `String` conversion functions
can be used to predictably and consistently convert numbers, we choose to
rely on `bignumber.js`, since `BN.js` only works with integers, but we need decimal
fractions for price calculations.

Problematic Number to String conversion cases:

```javascript
function fmts(n) {
    return {
        toString: n.toString(),
        toExponential: n.toExponential(),
        toFixed: n.toFixed(20),
        toPrecision: n.toPrecision(21)
    }
}

> fmts(1.23)
{ toString: '1.23',
  toExponential: '1.23e+0',
  toFixed: '1.22999999999999998224',
  toPrecision: '1.22999999999999998224' }

> fmts(1e-7)
{ toString: '1e-7',
  toExponential: '1e-7',
  toFixed: '0.00000010000000000000',
  toPrecision: '9.99999999999999954748e-8' }
```

We would use `BigNumber` instances for our source-of-truth kind of data
and would only convert them to String (and fixed-point integers) when
they are passed to `web3`, stored in browser `localStorage` or sent over
the network (trade orders thru whisper).

### Orderbook

Orderbook is assumed to be an object that implements the same state management
interface as [Vuex]{https://vuex.vuejs.org}.

### mocha.opts

`mocha.opts` shouldn't contain wildcard filenames because if any of
those files have syntax errors, the whole mocha process fails,
even if there were only individual test files specified on the command line
explicitly (by an automatically generated IntelliJ Mocha run configuration for example)

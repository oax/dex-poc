import Blotter from './Blotter.js'
import Navbar from './Navbar.js'
import Marketplace from './Marketplace.js'
import PriceChart from './PriceChart.js'
import OrderBook from './OrderBook.js'
import TradeHistory from './TradeHistory.js'
import Tutorial from './Tutorial.js'
import OrderForm from './OrderForm.js'

export default {
  template: `<div class="market">
    <marketplace/>
    <price-chart />
    <order-form/>
    <order-book />
    <blotter/>
    <trade-history/>
</div>
    `,

  components: {
    Blotter,
    Navbar,
    Marketplace,
    PriceChart,
    OrderBook,
    TradeHistory,
    Tutorial,
    OrderForm
  }
}

import Account from './Account.js'
import Wrapeth from './Wrapeth.js'

export default {
  name: 'tutorial',

  // language=HTML
  template: `
    <div class="tutorial">
      <div class="tutorial-container">

        <h1 style="font-size: 52px; text-align: center; margin: 0">Welcome to OAX DEX!</h1>
        <p style="font-size: 20px; text-align: center;">
          Read through the following instructions to understand better if you come to our site the first time 
        </p>
        <div style="height: 10px; background-color: white; margin:10px 0;"></div>
        
        <div id="box-no-meta" class="tutorial-box open"  v-if="getSignInError !== null && getSignInError==='NO_METAMASK'">
          <h2 class="tutorial-title">METAMASK NOT INSTALLED</h2>
          <p class="tutorial-description">We didn’t detect Metamask. Please install it to continue using OAX DEX.</p>
          <div>
            <a href="https://metamask.io/">
              <img src="images/metamask.png">
            </a>
          </div>
        </div>
        <div class="tutorial-content" style="display: none;">
          <a href="https://metamask.io/">Metamask</a>
        </div>


        <div id="box-no-meta" class="tutorial-box open"  v-if="getSignInError !== null && getSignInError==='WRONG_NETWORK'">
          <h2 class="tutorial-title">Connect to the right network</h2>
          <p class="tutorial-description">The current OAX DEX is still in Beta, and running on {{requiredNetwork.name}}, please connect your Metamask to it.</p>
         
        </div>
        <div class="tutorial-content" style="display: none;">
        </div>
        
        <div id="box-meta-locked" class="tutorial-box open "  v-if="getSignInError !== null && getSignInError==='METAMASK_LOCKED'">
          <h2 class="tutorial-title">Sign in your Metamask wallet</h2>
          <p class="tutorial-description">Please sign in your Metamask wallet and choose the account you would like to trade.</p>
          <h2 hidden>Wrong Network</h2>
          <p hidden>You are connected to the wrong network. Switch to Ethereum Main in Metamask to continue using OAX DEX.</p>
        </div>

        <div class="tutorial-content" style="display: none;">
        </div>
        
        <div id="box-locking" class="tutorial-box" v-if="getSignInError == null" v-on:click="toggle('#box-locking')">
          <h2 class="tutorial-title">Approve WETH and any other tokens you’d like to trade</h2>
          <p class="tutorial-description">You must give OAX DEX the permission to settle your orders on your behalf, you can approve each token by toggleing the switch next to the balance.</p>
        </div>

        <div class="tutorial-content" style="display: none;">
          <account/>
        </div>

        <div id="box-wrapping" class="tutorial-box" v-if="getSignInError == null" v-on:click="toggle('#box-wrapping')">
          <h2 class="tutorial-title" >Wrap some Ether</h2>
          <p class="tutorial-description">Wrap some Ether to tradable tokens, and trade them with any tokens in OAX DEX, once you done, you unwrap them back to normal Ether of the same value.</p>
        </div>

        <div class="tutorial-content" style="display: none;">
          <wrapeth/>
        </div>
        
        <div id="box-starting" class="tutorial-box" v-if="getSignInError == null" >
          <h2 class="tutorial-title">Start trading!</h2>
          <a  class="tutorial-description" style="color: black" href="#" @click.prevent="closeTutorial">Now that you’ve approved your tokens and wrapped some ether. Enjoy!</a>
        </div>
            <div class="tutorial-content" style="display: none;">
        </div>
      </div>
    </div>`,

  data: function () {
    return {
      currentStep: ''
    }
  },

  methods: {
    // use id to track current opened box
    toggle (id) {
      let box = this.$el.querySelector(id)
      let content = this.$el.querySelector(`${id} + .tutorial-content`)

      if (box.classList.contains('open')) {
        box.classList.remove('open')
        content.style.display = 'none'
      } else {
        if (this.currentStep !== '') {
          let preBox = this.$el.querySelector(this.currentStep)
          let preContent = this.$el.querySelector(`${this.currentStep} + .tutorial-content`)

          preBox.classList.remove('open')
          preContent.style.display = 'none'
        }
        box.classList.add('open')
        content.style.display = ''
      }

      this.currentStep = id
    },

    closeTutorial () {
      window.localStorage.setItem('skipTutorial', 'yes')
      this.$router.push({name: 'market'})
    }
  },

  computed: Vuex.mapGetters('metamaskUtils', [
    'getCurrentAccount', 'getSignInError', 'requiredNetwork'
  ]),

  components: {
    Account,
    Wrapeth
  }
}

import Navbar from './Navbar.js'

export default {
  template: `<div class="app">
    <navbar/>
    <div class="main-content">
      <router-view></router-view>
    </div>
</div>
    `,

  components: {
    Navbar
  }
}

/**
 * Formats a number with comma separators
 */
Vue.filter('commaSeparator', function (value) {
  const strOfValue = String(value)
  return strOfValue.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')
})

import { now } from '../../lib/chain-dsl/es6.js'

// Browsers store delays as a 32-bit signed integer
// see https://developer.mozilla.org/ro/docs/Web/API/window.setTimeout
const DELAY_LIMIT = 2147483647

const MAX_EXPIRY_DAYS_IN_MS = 60 * 60 * 24 * 90 * 1000

export function OrderExpiry (orderSource, scheduler = Rx.Scheduler.async) {
  return orderSource.delayWhen(order => {
    const orderTTL = Math.max((order.exp - now()) * 1000, 0)
    const effectiveTTL = Math.min(orderTTL, MAX_EXPIRY_DAYS_IN_MS)

    const delayChunks = Math.trunc(effectiveTTL / DELAY_LIMIT)
    const remainingDelay = effectiveTTL - delayChunks * DELAY_LIMIT

    const delays = R.times(
      () => Rx.operators.delay(DELAY_LIMIT, scheduler),
      delayChunks
    )

    delays.push(Rx.operators.delay(remainingDelay, scheduler))

    return Rx.Observable.of(order).pipe(...delays)
  })
}

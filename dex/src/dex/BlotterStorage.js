import * as Order from './Order.js'
import * as Fill from './Fill.js'

export function create (orderStorageOpts) {
  return {
    ...orderStorageOpts,

    unsubscribe () {},

    init () {
      const {store, storage, keyPrefix, addOrder, saveOn, statePath, netId} = this
      const key = `${keyPrefix}-${netId}`
      this.unsubscribe = store.subscribe((mutation, state) => {
        if (saveOn.includes(mutation.type)) {
          save({key, storage, state, statePath})
        }
      })

      restore({key, addOrder, saveOn, store, storage, netId})
      return {...this, key}
    },

    destroy () { this.unsubscribe() }
  }
}

export function load ({key, storage}) {
  const orders = JSON.parse(storage.getItem(key))

  if (!Array.isArray(orders)) {
    return []
  } else {
    return orders.map(wrappedRecord => {
      if (wrappedRecord.type === 'order') {
        wrappedRecord.data = Order.create(wrappedRecord.data)
      } else if (wrappedRecord.type === 'fill') {
        wrappedRecord.data = Fill.create(wrappedRecord.data)
      } else {
        throw Error('Invalid blotter record type')
      }

      return wrappedRecord
    })
  }
}

function restore (orderStorage) {
  const {key, addOrder, /* saveOn, */ store, storage} = orderStorage
  load({key, storage}).forEach(wrappedRecord => {
    try {
      store.commit(addOrder, wrappedRecord.data)
    } catch (err) {
      console.error(err)
    }
  })
}

function save ({key, state, storage, statePath}) {
  const blotterRecords = R.path(statePath, state)
  storage.setItem(key, JSON.stringify(blotterRecords))
}

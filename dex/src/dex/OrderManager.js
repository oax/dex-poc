import {sleep} from '../lib/chain-dsl/es6.js'
import * as Order from './Order.js'
import * as Orderbook from './Orderbook.js'
import * as Blotter from './Blotter.js'
import { getPastEvents } from './ContractLog.js'
import { VuexMutations } from './streams/VuexMutations.js'

export function create ({store, blotter, contracts, web3}) {
  return {
    store,
    blotter,
    contracts,
    orderTransceiver: null,
    orderbook: null,
    contractEvents: null,
    subscriptions: [],
    web3,
    exchange: contracts.xchg
  }
}

export function start (manager) {
  const processes = [
    updateBlotterFromContractEvents,
    updateOrderbookFromContractEvents,
    monitorFillIntentEvents
  ]

  const subscriptions = processes.map(p => p(manager))
  return {...manager, ...{subscriptions}}
}

export function stop (manager) {
  manager.subscriptions.forEach(s => s.unsubscribe())
}

function updateOrderbookFromContractEvents (manager) {
  const {
    store,
    orderbook,
    contractEvents,
    contracts
  } = manager

  return contractEvents.subscribe(async event => {
    const orderHash = event.returnValues.orderHash

    const makerOrder = Orderbook.get(orderbook, orderHash)

    if (!makerOrder) { return }

    const updatedMakerOrder = await Order.updateFromEvent(event, makerOrder, contracts)

    if (['fulfilled', 'expired', 'cancelled'].includes(updatedMakerOrder.status)) {
      store.commit('removeFromOrderbook', updatedMakerOrder)
    } else {
      store.commit('updateInOrderbook', updatedMakerOrder)
    }
  })
}

// MOVE BELOW FUNCTIONALITY TO BLOTTER

function updateBlotterFromContractEvents ({contractEvents, store, blotter, contracts}) {
  return contractEvents.subscribe(event => {
    const orderHash = event.returnValues.orderHash

    // FIXME: Optimize record lookup
    const myRecords = Blotter.orders(blotter).filter(record => {
      return record.recordHash === orderHash
    })

    myRecords
      .filter(record => record.type === 'order')
      .forEach(async record => {
        const updatedOrder = await Order.updateFromEvent(event, record.data, contracts)
        store.commit('blotter/updateRecord', updatedOrder)
      })
  })
}

function monitorFillIntentEvents ({web3, exchange, store, contracts}) {
  const subscription = VuexMutations(store, ['blotter/addRecord'])
    .subscribe(async commit => {
      const fill = commit.mutation.payload

      const wrappedFill = Blotter.toRecord(fill)

      if (wrappedFill.type === 'fill') {
        let tx

        do {
          tx = await web3.eth.getTransaction(fill.transactionHash)
          await sleep(100)
        } while (R.isNil(tx.blockNumber))

        const eventSub = (await contractEventsForFillIntent({web3, exchange, fill})).subscribe(
          async event => {
            const updatedOrder = await Order.updateFromEvent(event, fill, contracts)
            store.commit('blotter/updateRecord', updatedOrder)
          }
        )
        subscription.add(eventSub)
      }
    })

  return subscription
}

async function contractEventsForFillIntent ({web3, exchange, fill}) {
  const tx = await web3.eth.getTransaction(fill.transactionHash)
  const blockNumber = tx.blockNumber

  return getPastEvents(exchange, 'allEvents', {
    filter: {orderHash: fill.fillIntent.order.orderHash},
    fromBlock: blockNumber,
    toBlock: 'latest'
  })
    .mergeMap(events => Rx.Observable.from(events))
    .filter(event => event.transactionHash === fill.transactionHash)
}

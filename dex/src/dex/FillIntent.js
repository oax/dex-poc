
export class FillIntent {
  constructor ({order, address, buyAmt, sellAmt}) {
    this.order = order
    this.address = address
    this.buyAmt = buyAmt
    this.sellAmt = sellAmt

    Object.freeze(this)
  }
}

export function create (params) {
  return new FillIntent(params)
}

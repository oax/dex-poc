import { superstruct } from '../lib/superstruct/lib/index.es.js'

export const Assert = {
  ok (truthy, message) {
    if (!truthy) throw new Error(message || 'Assertion failure')
  },
  strictEqual (actual, expected, message) {
    if (actual !== expected) {
      throw new Error(message ||
        `${actual} !== ${expected}`)
    }
  },
  equal (actual, expected, message) {
    if (actual != expected) { // eslint-disable-line eqeqeq
      throw new Error(message ||
        `${actual} !== ${expected}`)
    }
  }
}

// The `defineProperty` idea is based on
//     https://github.com/vuejs/vue/issues/2637
//
export function dontObserve (obj, prop) {
  // Mutate `obj`
  Object.defineProperty(obj, prop, {
    configurable: false,
    enumerable: true
  })

  // `obj` refers to the same `obj` as the input, it's just been mutated
  return obj
}

export function mapMutation (mutation) {
  return (facade, ...args) => facade.store.commit(mutation, ...args)
}

export const devChainUrl = 'http://localhost:8900'
export const anotherDevChainUrl = 'http://localhost:8800'

export function makeChannel () {
  return new Rx.Subject()
}

export function tokenPairToHash (tokenPair) {
  return tokenAddressesToHash([
    tokenPair.base.address,
    tokenPair.quote.address
  ])
}

export function tokenAddressesToHash (tokenAddresses) {
  return Web3.utils.sha3(
    tokenAddresses
      .slice(0, 2)
      .sort()
      .join('')
  )
}

/**
 * Custom Superstruct validation types for DEX
 *
 * @see {@link https://github.com/ianstormtaylor/superstruct/blob/master/docs/guide.md#defining-custom-data-types}
 */
export const struct = superstruct({
  types: {
    orderAmount: value => BigNumber.isBigNumber(value) && value.gt(D`0`),
    hash: value => /^0x[0-9a-f]{64}$/i.test(value),
    address: value => Web3.utils.isAddress(value),
    signature: value => /^0x[0-9a-f]+$/i.test(value),
    hex: value => Web3.utils.isHex(value)
  }
})

export function isWeb3ProviderSubscriptionCapable (web3) {
  return typeof web3._requestManager.provider.on === 'function'
}

const {address, send, create} = require('chain-dsl')
const {load} = require('chain-dsl/json')
const solc = require('chain-dsl/solc')

exports.base = async function base (web3, compiledContracts, DEPLOYER) {
  const deploy = (contract, ...args) => create(web3, DEPLOYER, contract,
    ...args)

  const {
    Exchange,
    ZRXToken,
    WETH9,
    DemoOAX,
    DemoSWIMUSD,
    TokenTransferProxy
  } = compiledContracts

  const zrx = await deploy(ZRXToken)
  const weth = await deploy(WETH9)
  const oax = await deploy(DemoOAX)
  const swimusd = await deploy(DemoSWIMUSD)
  const proxy = await deploy(TokenTransferProxy)
  const xchg = await deploy(Exchange, address(zrx), address(proxy))
  await send(proxy, DEPLOYER, 'addAuthorizedAddress', address(xchg))

  return {xchg, zrx, weth, oax, swimusd, proxy}
}

/*
* Convenience function to load compiled smart contract code and
* deploy all of them and assemble them into a system.
*/
exports.all = async function all (web3, compiledContractsJsonFile, DEPLOYER) {
  const compiledContracts = solc.flattenJsonOutput(
    load(compiledContractsJsonFile))
  return exports.base(web3, compiledContracts, DEPLOYER)
}

const {address, send, create, transfer} = require('../index')
const {load} = require('../json')
const solc = require('../solc')

async function base(web3, compiledContracts, DEPLOYER) {
    const deploy = (contract, ...args) => create(web3, DEPLOYER, contract, ...args)

    const foo = await deploy(compiledContracts.Foo)

    return {foo}
}

/*
* Convenience function to load compiled smart contract code and
* deploy all of them and assemble them into a system.
*/
async function all(web3, compiledContractsJsonFile, DEPLOYER) {
    const compiledContracts = solc.flattenJsonOutput(load(compiledContractsJsonFile))
    const {foo} = await base(web3, compiledContracts, DEPLOYER)
    return {foo}
}

module.exports = {base, all}

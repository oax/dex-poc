import Chai from "chai"
import "../index" // Setup globals

const {Assertion} = Chai
Chai.use(require('chai-subset'))
Chai.use(require('chai-as-promised'))

Assertion.addMethod('eqD', function (N, dp) {
    let actual = D(this._obj)
    let expected = D(N)
    if (dp !== null && typeof dp !== 'undefined') {
        actual = actual.precision(dp, BigNumber.ROUND_HALF_UP)
        expected = expected.precision(dp, BigNumber.ROUND_HALF_UP)
    }
    this.assert(actual.eq(expected),
        'expected #{act} to equal #{exp}',
        'expected #{act} NOT to equal #{exp}',
        expected.toString(), actual.toString())
})

export const expect = Chai.expect

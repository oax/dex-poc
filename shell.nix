# To update:
#    curl -sI https://nixos.org/channels/nixpkgs-unstable/nixexprs.tar.xz | awk '/Location:/ {print $2}'
# then compute the hash with
#    nix-prefetch-url --type sha256 --unpack URL
with import (
  builtins.fetchTarball {
    url = "https://d3g5gsiof5omrk.cloudfront.net/nixpkgs/nixpkgs-18.09pre144939.14a9ca27e69/nixexprs.tar.xz";
    sha256 = "1izvvfyxjw4wxs1g1swvixwb7b0v7abm1gnp8gb24b0q3921569n";
  }
) {};

let
  nodejs = nodejs-8_x;
  geth = (lib.getBin go-ethereum);

in mkShell rec {
  buildInputs = [
    solc geth nodejs nodePackages_8_x.pnpm
    overmind entr grc jq coreutils python direnv
    es curl symlinks # for dex only
    git # for CI
  ];

  shellHook = ''
    export PATH="$PATH:$PWD/dex/bin"
    print_module_version="console.log(process.versions.modules)"
    export npm_config_store=''${NPM_STORE_PREFIX-$HOME}/.pnpm-store-abi-$(${nodejs}/bin/node -e $print_module_version)
    '';
}
